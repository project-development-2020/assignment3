Name - Tushar Bhutani

App Description - 

Hardware store application to check and buy home hardware products from the application and make a curbside order.


User instruction - 

User need to say Hi and check the product details by clicking the URL as shown in the details.
After that, user enters the product number in the Chat to order that or can directly exit from the app if don't want to order anything.
Then the application asks user to check the up-sell items and if anything user want from the menu, user can enter the product number
and the application process the order with all the details including order and total amount to pay.


User instruction - How to build a project.

1. Download the project from the repository.

2. Install visual studio code from below link.
https://code.visualstudio.com/download

3. Once the visual studio is installed, Open the folder by clicking the open folder and enter the path of the folder.

4. Open the Bash terminal by clicking the option(New terminal).

5. Install npm, command  - npm install

6. Start the npm, command - npm start.
It will start and show a port at the end. Default port - 3002

7. Open any browser, for better experience open Chrome or Mozilla.

8. Enter the below URL in browser.
localhost:{Port} - (Example - locahost:3002)

Usually the default port is 3002 but if it starts with any other port, enter that port instead of 3002.

9. Your application starts. Say Hi to see the instructions.

