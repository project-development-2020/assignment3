const Order = require("./Order");

const OrderState = Object.freeze({
    WELCOMING:   Symbol("welcoming"),
    HomeH:   Symbol("homeh"),
    LITTER:   Symbol("litter"),
    EXTRAS:  Symbol("extras"),
    ITEM:   Symbol("item"),
    UPSELL: Symbol("upsell"),
    TAX: Symbol("tax")
});

module.exports = class LockDownEssentials extends Order{
    constructor(sNumber, sUrl){
        super(sNumber, sUrl);
        this.stateCur = OrderState.WELCOMING;
        this.sSpecies = "";
        this.sFood = "";
        this.sLitter = "";
        this.sExtras = "";
        this.sInput="";
        this.sItem="";
        this.nTotal = 0;
        this.sStatement="";
    }
    handleInput(sInput){
        let aReturn = [];
        this.sStatement = "We also have a selection of car cloths, ear-buds, geeky headlamps.\n Enter item number as listed below \n 1. Car cloth - $2.99 \n 2. Ear-buds - $1.99 \n 3. Geeky headlamps - $3.99 \n or No";
        switch(this.stateCur){
            case OrderState.WELCOMING:
                this.stateCur = OrderState.ITEM;
                aReturn.push("Welcome to Tushar's Home Hardwares.");
                aReturn.push(`For a list of what we sell tap:`);
                aReturn.push(`${this.sUrl}/payment/${this.sNumber}/`);
                aReturn.push("Enter the item number you want to buy ");
                this.sItem = sInput;
                break;
            case OrderState.ITEM:
                if(sInput == 1){
                    this.sItem="Broom";
                    this.nTotal += 10.99;
                    this.stateCur = OrderState.UPSELL;
                    aReturn.push(`${this.sStatement}`);
                  }
                else if(sInput == 2){
                    this.sItem="Dustbin";
                    this.nTotal += 7.99
                    this.stateCur = OrderState.UPSELL;
                    aReturn.push(`${this.sStatement}`);
                    
                  }
                else if(sInput == 3){
                    this.sItem="Lightbulb";
                    this.nTotal += 3.99;
                    this.stateCur = OrderState.UPSELL;
                    aReturn.push(`${this.sStatement}`);
                    
                  }
                else if(sInput == 4){
                    this.sItem="Garbage Bags";
                    this.nTotal += 10.99
                    this.stateCur = OrderState.UPSELL;
                    aReturn.push(`${this.sStatement}`);
                  }
                  else {
                    aReturn.push("Please enter valid input.");
                    this.stateCur = OrderState.WELCOMING;
                  }
                  break;
            case OrderState.UPSELL:
                this.stateCur = OrderState.TAX;
                this.sExtras = sInput;
                if(sInput == 1){
                    this.sExtras="Car Cloth";
                    this.nTotal += 2.99;
                    this.stateCur = OrderState.TAX;
                    
                  }else if(sInput == 2){
                    this.sExtras="Ear-buds";
                    this.nTotal += 1.99;
                    this.stateCur = OrderState.TAX;
                    
                  }else if(sInput == 3){
                    this.sExtras="Geeky headlamps";
                    this.nTotal += 3.99;
                    this.stateCur = OrderState.TAX;
                  }
                  else {
                    this.nTotal += 0;
                  }
                aReturn.push("Thank-you for your order of");
                aReturn.push(`${this.sItem} and ${this.sExtras} - Extra item`);
                this.nTotal = this.nTotal * 1.13;
                this.nTotal = this.nTotal.toFixed(2);
                aReturn.push(`Your total comes to $${this.nTotal} after tax`);
                aReturn.push(`We will text you from 519-777-7777 when your order is ready or if we have questions.`)
                this.isDone(true);
                break;
        }
        return aReturn;
    }
    renderForm(){
      // your client id should be kept private
      return(`
      <html>

<head>
    <meta content="text/html; charset=UTF-8" http-equiv="content-type">
    <style type="text/css">
        ol {
            margin: 0;
            padding: 0
        }

        table td,
        table th {
            padding: 0
        }

        .c8 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 1pt;
            border-right-width: 1pt;
            border-left-color: #000000;
            vertical-align: top;
            border-right-color: #000000;
            border-left-width: 1pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 1pt;
            width: 83.2pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c7 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 1pt;
            border-right-width: 1pt;
            border-left-color: #000000;
            vertical-align: top;
            border-right-color: #000000;
            border-left-width: 1pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 1pt;
            width: 166.5pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c12 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 1pt;
            border-right-width: 1pt;
            border-left-color: #000000;
            vertical-align: top;
            border-right-color: #000000;
            border-left-width: 1pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 1pt;
            width: 195.8pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c6 {
            color: #000000;
            font-weight: 400;
            text-decoration: none;
            vertical-align: baseline;
            font-size: 12pt;
            font-family: "Arial";
            font-style: normal
        }

        .c2 {
            color: #000000;
            font-weight: 400;
            text-decoration: none;
            vertical-align: baseline;
            font-size: 11pt;
            font-family: "Arial";
            font-style: normal
        }

        .c4 {
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.15;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        .c10 {
            color: #000000;
            font-weight: 400;
            text-decoration: none;
            vertical-align: baseline;
            font-family: "Arial";
            font-style: normal
        }

        .c0 {
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.15;
            orphans: 2;
            widows: 2;
            text-align: center
        }

        .c14 {
            margin-left: 21.8pt;
            border-spacing: 0;
            border-collapse: collapse;
            margin-right: auto
        }

        .c1 {
            background-color: #ffffff;
            max-width: 468pt;
            padding: 72pt 72pt 72pt 72pt
        }

        .c13 {
            height: 42.4pt
        }

        .c3 {
            height: 38.6pt
        }

        .c11 {
            font-size: 12pt
        }

        .c9 {
            font-size: 18pt
        }

        .c15 {
            height: 39.8pt
        }

        .c5 {
            height: 11pt
        }

        .title {
            padding-top: 0pt;
            color: #000000;
            font-size: 26pt;
            padding-bottom: 3pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        .subtitle {
            padding-top: 0pt;
            color: #666666;
            font-size: 15pt;
            padding-bottom: 16pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        li {
            color: #000000;
            font-size: 11pt;
            font-family: "Arial"
        }

        p {
            margin: 0;
            color: #000000;
            font-size: 11pt;
            font-family: "Arial"
        }

        h1 {
            padding-top: 20pt;
            color: #000000;
            font-size: 20pt;
            padding-bottom: 6pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        h2 {
            padding-top: 18pt;
            color: #000000;
            font-size: 16pt;
            padding-bottom: 6pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        h3 {
            padding-top: 16pt;
            color: #434343;
            font-size: 14pt;
            padding-bottom: 4pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        h4 {
            padding-top: 14pt;
            color: #666666;
            font-size: 12pt;
            padding-bottom: 4pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        h5 {
            padding-top: 12pt;
            color: #666666;
            font-size: 11pt;
            padding-bottom: 4pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        h6 {
            padding-top: 12pt;
            color: #666666;
            font-size: 11pt;
            padding-bottom: 4pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            font-style: italic;
            orphans: 2;
            widows: 2;
            text-align: left
        }
    </style>
</head>

<body class="c1">
    <p class="c4"><span class="c2">Welcome to Tushar&rsquo;s Home Hardware</span></p>
    <p class="c4 c5"><span class="c2"></span></p>
    <p class="c4"><span class="c2">For curbside pickup:</span></p>
    <p class="c4 c5"><span class="c2"></span></p>
    <p class="c4"><span class="c6">Text &ldquo;HH&rdquo; to 519-777-7777</span></p>
    <p class="c4 c5"><span class="c6"></span></p>
    <p class="c4 c5"><span class="c6"></span></p><a id="t.c6972d5a227017bfe56ebea94300fa5961ed5f7d"></a><a id="t.0"></a>
    <table class="c14">
        <tbody>
            <tr class="c15">
                <td class="c8" colspan="1" rowspan="1">
                    <p class="c0"><span class="c10 c9">Item No.</span></p>
                </td>
                <td class="c12" colspan="1" rowspan="1">
                    <p class="c0"><span class="c10 c9">Name</span></p>
                </td>
                <td class="c7" colspan="1" rowspan="1">
                    <p class="c0"><span class="c10 c9">Cost</span></p>
                </td>
            </tr>
            <tr class="c3">
                <td class="c8" colspan="1" rowspan="1">
                    <p class="c0"><span class="c10 c9">1</span></p>
                </td>
                <td class="c12" colspan="1" rowspan="1">
                    <p class="c0"><span class="c10 c9">Brooms</span></p>
                </td>
                <td class="c7" colspan="1" rowspan="1">
                    <p class="c0"><span class="c10 c9">&nbsp; 10.99 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            &nbsp;</span></p>
                </td>
            </tr>
            <tr class="c3">
                <td class="c8" colspan="1" rowspan="1">
                    <p class="c0"><span class="c9 c10">2</span></p>
                </td>
                <td class="c12" colspan="1" rowspan="1">
                    <p class="c0"><span class="c9">Dustbin</span><span class="c10 c9">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            &nbsp; &nbsp; &nbsp; </span></p>
                </td>
                <td class="c7" colspan="1" rowspan="1">
                    <p class="c0"><span class="c10 c9">&nbsp;7.99 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            &nbsp;</span></p>
                </td>
            </tr>
            <tr class="c13">
                <td class="c8" colspan="1" rowspan="1">
                    <p class="c0"><span class="c10 c9">3</span></p>
                </td>
                <td class="c12" colspan="1" rowspan="1">
                    <p class="c0"><span class="c9">Light bulbs</span><span class="c10 c9">&nbsp; &nbsp; &nbsp; &nbsp;
                            &nbsp; </span></p>
                </td>
                <td class="c7" colspan="1" rowspan="1">
                    <p class="c0"><span class="c10 c9">&nbsp; 3.99 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            &nbsp;</span></p>
                </td>
            </tr>
            <tr class="c13">
                <td class="c8" colspan="1" rowspan="1">
                    <p class="c0"><span class="c10 c9">4</span></p>
                </td>
                <td class="c12" colspan="1" rowspan="1">
                    <p class="c0"><span class="c10 c9">Garbage Bags</span></p>
                </td>
                <td class="c7" colspan="1" rowspan="1">
                    <p class="c0"><span class="c10 c9">&nbsp; &nbsp;10.99</span></p>
                </td>
            </tr>
        </tbody>
    </table>
    <p class="c4 c5"><span class="c6"></span></p>
    <p class="c4 c5"><span class="c6"></span></p>
    <p class="c4 c5"><span class="c6"></span></p>
    <p class="c4"><span class="c11">We also have a selection of car cloths, ear-buds, geeky headlamps.</span></p>
</body>

</html>      `);
  
    }
}
